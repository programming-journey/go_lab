# Golang Milestones

1. Console applications (to get feet wet)
- [x] Hello World
- [x] Anagram
- [x] Pizza Box (A Pizza Ordering simulation)

2. OOP principles (opt)
- [x] Constructor
- [ ] Modifiers
- [ ] Encapsulation
- [ ] Inheritance
- [x] Polymophism
- [x] Interface

3. Backend(opt)
- [x] RestAPI(MUX)
- [ ] RestAPI(Client)??
- [ ] RestAPI(GIN)
- [ ] GraphQL
- [ ] grpc

4. Requests(opt)
- [x] Login


5. GUI App
- [ ] Hello World
- [ ] Other Apps

6. Syntax
