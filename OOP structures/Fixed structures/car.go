package main

type car struct {
	year  int32
	mpg   int32
	speed int32
}

func (c *car) accelerate() int32 {
	return c.speed + 20
}

func (c *car) brake() int32 {
	return c.speed - 50
}

func newCar() *car {
	return &car{
		year:  2016,
		mpg:   20,
		speed: 100,
	}
}

func main() {
	car1 := newCar()
	println(car1.accelerate())
	println(car1.brake())
	println(car1.year)
	println(car1.mpg)
	println(car1.speed)
}
